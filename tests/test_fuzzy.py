﻿'''
Test cases for FwFuzzy/Fuzzy
'''
from __future__ import absolute_import
from FwFuzzy import Fuzzy


class TestFuzzy():
    def test_empty(self):
        o_fuzzy = Fuzzy([])
        assert not o_fuzzy.find('anything')
        assert not o_fuzzy.search('anything')
        
    def test_exact(self):
        o_fuzzy = Fuzzy(["one", "two", "three", "four", "five", "sixseven"])
        assert o_fuzzy.find('one') ==  "one"
        result = o_fuzzy.search('one')
        assert len(result) == 1
        assert result[0][0] == "one"
        assert result[0][1] == 1.0
        
    def test_acronym(self):
        o_fuzzy = Fuzzy(["one", "two", "three", "four", "five", "sixseven"])
        assert o_fuzzy.find('T.W.O.') ==  "two"
        result = o_fuzzy.search('T.W.O.')
        assert len(result) == 1
        assert result[0][0] == "two"
        assert result[0][1] >= 0.9
        
    def test_change(self):
        o_fuzzy = Fuzzy(["one", "two", "three", "four", "five", "sixseven"])
        assert o_fuzzy.find('threw') ==  "three"
        result = o_fuzzy.search('threw')
        assert len(result) == 1
        assert result[0][0] == "three"
        assert result[0][1] >= 0.75
    
    def test_transposition(self):
        o_fuzzy = Fuzzy(["one", "two", "three", "four", "five", "sixseven"])
        assert o_fuzzy.find('fuor') ==  "four"
        result = o_fuzzy.search('fuor')
        assert len(result) == 1
        assert result[0][0] == "four"
        assert result[0][1] >= 0.5 
    
    def test_phonetic(self):
        o_fuzzy = Fuzzy(["one", "two", "three", "four", "five", "sixseven"])
        assert o_fuzzy.find('phive') ==  "five"
        result = o_fuzzy.search('phive')
        assert len(result) == 1
        assert result[0][0] == "five"
        assert result[0][1] >= 0.5
        
    def test_partial(self):
        o_fuzzy = Fuzzy(["one", "two", "three", "four", "five", "sixseven"])
        assert o_fuzzy.find('six', d_threshold=0.5, b_starts_with=True, b_ends_with=True) ==  "sixseven"
        result = o_fuzzy.search('six', d_threshold=0.5, b_starts_with=True, b_ends_with=True)
        assert len(result) == 1
        assert result[0][0] == "sixseven"
        assert result[0][1] >= 0.5
        
#    def test_submatch(self):
#        o_fuzzy = Fuzzy(["one", "two", "three", "four", "five", "six", "seven"])
#        assert o_fuzzy.find('sixseven', d_threshold=0.5, b_starts_with=True, b_ends_with=True) ==  "seven"
#        result = o_fuzzy.search('sixseven', d_threshold=0.5, b_starts_with=True, b_ends_with=True)
#        assert len(result) == 2
#        assert result[0][0] == "seven"
#        assert result[0][1] >= 0.5
