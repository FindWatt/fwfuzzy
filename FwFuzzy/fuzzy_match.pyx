﻿from cpython cimport array
#import array
cimport numpy as np
import numpy as np

import re, math
from collections import defaultdict, Counter
from operator import itemgetter
#from time import time
import numpy as np
import pyphonetics
from pyphonetics.distance_metrics import levenshtein_distance, hamming_distance
from pyphonetics.exceptions import UnicodeException, DistanceMetricError

from FwText import (
    edit_distance, strip_unicode_accents,
    clean_text, remove_all_punctuation,
    pluralize, singularize, pluralize_tuple, singularize_tuple,
    all_ngrams_flat, char_ngrams)
from FwFreq import (
    sort_freqs, add_freq_dicts, herfindahl_freqs_dict,
    freq, jaccard)
from FwEditClassifier import edits, distance, sentence_distance

cdef dict e_CHAR_IMPORTANCE = {
    'e': <int>1, 'E': <int>11,
    'a': <int>1, 'A': <int>11,
    'i': <int>2, 'I': <int>12,
    'o': <int>3, 'O': <int>13,
    'u': <int>3, 'U': <int>13,
    'y': <int>4, 'Y': <int>14,
    'r': <int>25, 'R': <int>35,
    'l': <int>26, 'L': <int>36,
    's': <int>27, 'S': <int>37,
    'z': <int>27, 'Z': <int>37,
    'c': <int>28, 'C': <int>38,
    'k': <int>28, 'K': <int>38,
    'q': <int>29, 'Q': <int>39,
    'd': <int>31, 'D': <int>41,
    'b': <int>31, 'B': <int>41,
    'p': <int>31, 'P': <int>41,
    't': <int>31, 'T': <int>41,
    'm': <int>32, 'M': <int>42,
    'n': <int>32, 'N': <int>42,
    'h': <int>32, 'H': <int>42,
    'g': <int>33, 'G': <int>43,
    'j': <int>33, 'J': <int>43,
    'f': <int>34, 'F': <int>44,
    'v': <int>35, 'V': <int>45,
    'w': <int>35, 'W': <int>45,
    'x': <int>36, 'X': <int>46,
    '0': <int>50,
    '1': <int>51,
    '2': <int>52,
    '3': <int>53,
    '4': <int>54,
    '5': <int>55,
    '6': <int>56,
    '7': <int>57,
    '8': <int>58,
    '9': <int>59,
}

cdef set c_IMPORTANT = set('0123456789Xx')
cdef set c_IMPORTANT_CHANGE = set('0123456789Xx')
cdef set c_OTHER = set('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrtuvwxyz.')
cdef set c_UNIMPORTANT = set('s _-,()\'"')

re_PUNCT = re.compile(r"[^A-Za-z0-9]")

cdef list a_SPLIT_POINTS = [get_split_points(i_len) for i_len in range(100)]
a_SPLIT_POINTS[1] = ((0, 1), (0, 0), (0, 0), (0,1))
a_SPLIT_POINTS[2] = ((0, 1), (1, 1), (1, 2), (1, 2))
a_SPLIT_POINTS[3] = ((0, 2), (1, 2), (1, 3), (2, 3))
a_SPLIT_POINTS[4] = ((0, 2), (1, 3), (2, 4), (3, 4))

cdef list a_MIN_OVERLAP = [  #(i_min_overlap, d_min_overlap)
    (0, 0.0), # 0 char
    (1, 0.0), # 1 char
    (1, 0.0), # 2 chars
    (1, 0.1), # 3 chars
    (1, 0.1), # 4 chars
    (2, 0.2), # 5 chars
    (2, 0.30), # 6 chars
    (3, 0.30), # 7 chars
    (3, 0.30), # 8 chars
    (4, 0.40), # 9 chars
    (4, 0.40), # 10 chars
    (5, 0.40), # 11 chars
    (5, 0.40), # 12 chars
    (5, 0.40), # 13 chars
    (6, 0.40), # 14 chars
    (6, 0.40), # 15 chars
    (6, 0.40), # 16 chars
    (6, 0.40), # 17 chars
    (7, 0.40), # 18 chars
    (7, 0.40), # 19 chars
    (7, 0.40), # 20 chars
    (7, 0.40), # 21 chars
    (7, 0.40), # 22 chars
    (8, 0.40), # 23 chars
    (8, 0.40), # 24 chars
]

cdef class Metaphone:
    """ The metaphone algorithm.
        [Reference]: https://en.wikipedia.org/wiki/Metaphone
        [Author]: Lawrence Philips, 1990 """
    cdef list rules
    cdef dict distances
    def __init__(self):
        self.distances = {
            'levenshtein': levenshtein_distance,
            'hamming': hamming_distance,}

        self.rules = [
            (r'[^a-z]', r''),
            (r'([bcdfhjklmnpqrstvwxyz])\1+', r'\1'),
            (r'^ae', r'e'),
            (r'^[gkp]n', r'n'),
            (r'^wr', r'r'),
            (r'^x', r's'),
            (r'^wh', r'w'),
            (r'mb$', r'm'),
            (r'(?!^)sch', r'sk'),
            (r'[aeiou]nght', 'ngth'),
            #(r'th', r'0'),
            (r't?ch|sh', r'x'),
            (r'c(?=ia)', r'x'),
            (r'[st](?=i[ao])', r'x'),
            (r's?c(?=[iey])', r's'),
            (r'([cq]k*|(?<![sn])k(\b|$))', r'kk'),
            (r'dg(?=[iey])', r'j'),
            (r'd', r't'),
            (r'g(?=h[^aeiou])', r''),
            (r'gn(ed)?', r'n'),
            (r'([^g]|^)g(?=[iey])', r'\1j'),
            (r'g+', r'k'),
            (r'ph', r'f'),
            (r'([aeiou])h(?=\b|[^aeiou])', r'\1'),
            (r'[wy](?![aeiou])', r''),
            (r'z', r's'),
            (r'v', r'f'),
            (r'(?!^)[aeiou]+', r'')
        ]
        self.rules = [
            (re.compile(s_pattern), s_substitute)
            for s_pattern, s_substitute in self.rules
        ]
    
    cpdef str phonetics(self, str word):
        #if not isinstance(word, str):
        #    raise UnicodeException('Expected a unicode string!')
        cdef str code, s_sub
        
        #code = unidecode(word).lower()
        code = strip_unicode_accents(word).lower()
        
        for re_rule, s_sub in self.rules:
            #code = re.sub(item[0], item[1], code)
            code = re_rule.sub(s_sub, code)
        return code
    
    cpdef bint sounds_like(self, str word1, str word2):
        """Compare the phonetic representations of 2 words, and return a boolean value."""
        return self.phonetics(word1) == self.phonetics(word2)
    
    cpdef int distance(self, str word1, str word2, str metric='levenshtein'):
        """Get the similarity of the words, using the supported distance metrics."""
        if metric in self.distances:
            distance_func = self.distances[metric]
            return distance_func(self.phonetics(word1), self.phonetics(word2))
        else:
            raise DistanceMetricError(
                'Distance metric not supported! Choose from levenshtein, hamming.')

#o_METAPHONE = pyphonetics.Metaphone()
o_METAPHONE = Metaphone()


cpdef ((int, int), (int, int), (int, int), (int, int)) get_split_points(int i_len):
    ''' Get the indexes to split a text into start, start-middle, middle-end, and end
        Arguments:
            i_len: {int} the number of characters in 
        Returns:
            {tuple} of tuples of positions
            example: for i_len==5 return ((0, 2), (1, 3), (2, 4), (3, 5))
    '''
    cdef (int, int) t_start, t_start_mid, t_mid_end, t_end
    t_start = (0, max(1, int(i_len * 0.25 + 0.5)))
    t_start_mid = (int(i_len * 0.20), int(i_len * 0.5 + 0.5))
    t_mid_end = (int(i_len * 0.5), int(i_len * 0.80 + 0.5))
    t_end = (int(i_len * 0.75), i_len)
    return t_start, t_start_mid, t_mid_end, t_end


cpdef tuple split_text(str s_text, tuple t_start, tuple t_start_mid, tuple t_mid_end, tuple t_end):
    ''' Split a text into start, start-middle, middle-end, and end pieces using passed in position tuples
        Arguments:
            s_text: {str} the text to split
            t_start: {tuple} the (start:end) position index for the start piece
            t_start_mid: {tuple} the (start:end) position index for the start_mid piece
            t_mid_end: {tuple} the (start:end) position index for the mid_end piece
            t_end: {tuple} the (start:end) position index for the end piece
        Returns:
            {tuple} of tuples of {str} text pieces
            example: for s_text="abcde" return ('ab', 'bc', 'cd', 'de')
    '''
    return (
        s_text[t_start[0]: t_start[1]],
        s_text[t_start_mid[0]: t_start_mid[1]],
        s_text[t_mid_end[0]: t_mid_end[1]],
        s_text[t_end[0]: t_end[1]])


cpdef (int, int, int) count_important_edits(tn_edit):
    ''' Count the important, misc, and unimportant edits
        Arguments:
            tn_edit: {namedtuple} from FwEdditClassifier.edits()
        Returns
            {tuple} of {int} edit counts: (i_important, i_other, i_unimportant)
    '''
    cdef int i_important, i_other, i_unimportant
    cdef str s_char, s_char1, s_char2

    i_important, i_other, i_unimportant = 0, 0, 0
    for s_char in tn_edit.added:
        if s_char in c_IMPORTANT:
            i_important += 1
        elif s_char in c_UNIMPORTANT:
            i_unimportant += 1
        else:
            i_other += 1

    for s_char in tn_edit.removed:
        if s_char in c_IMPORTANT:
            i_important += 1
        elif s_char in c_UNIMPORTANT:
            i_unimportant += 1
        else:
            i_other += 1

    for s_char1, s_char2 in tn_edit.changed:
        if s_char1 in c_IMPORTANT_CHANGE or s_char2 in c_IMPORTANT_CHANGE:
            i_important += 1
        elif s_char1 in c_UNIMPORTANT and s_char2 in c_UNIMPORTANT:
            i_unimportant += 1
        else:
            i_other += 1

    for s_char1, s_char2 in tn_edit.transposed:
        i_other += 1

    return i_important, i_other, i_unimportant


cpdef np.ndarray count_chars(str s_text, bint b_case_insensitive=True, dtype=int):
    ''' Count the instance of the numbers and letters in a clean, lowercase ascii string
        Arguments:
            s_text: {str} to count chars of
            b_case_insensitive: {bool} whether to count "a" and "A" as the same
            d_type: {type} the number data type to use for the count variables.
                    You can use standard int, or numpy np.uint8 or np.uint16 for lower memory usage
        Returns:
            {np.ndarray} 2d array of ascii ord index, total/start/begin/begin-mid/mid-end/end counts
    '''
    cdef np.ndarray np_row_counts, np_char
    cdef str char
    cdef int idx, i_len, i_char
    #cdef (int, int) t_begin, t_begin_mid, t_mid_end, t_end
    cdef tuple t_begin, t_begin_mid, t_mid_end, t_end

    i_len = len(s_text)
    if i_len < 100:
        t_begin, t_begin_mid, t_mid_end, t_end = a_SPLIT_POINTS[i_len]
    else:
        t_begin, t_begin_mid, t_mid_end, t_end = get_split_points(i_len)
    
    if b_case_insensitive:
        np_row_counts = np.zeros(shape=(36, 5), dtype=dtype)
        for idx, char in enumerate(s_text):
            i_char = ord(char)
            
            if 97 <= i_char < 123:
                i_char -= 87
            elif 65 <= i_char < 91:
                i_char -= 55
            elif 48 <= i_char < 58:
                i_char -= 48
            else:
                continue

            np_char = np_row_counts[i_char]
            np_char[0] += 1
            if idx < t_begin[1]:
                np_char[1] += 1
            if t_begin_mid[0] <= idx < t_begin_mid[1]:
                np_char[2] += 1
            if t_mid_end[0] <= idx < t_mid_end[1]:
                np_char[3] += 1
            if t_end[0] <= idx:
                np_char[4] += 1
    
    else:
        np_row_counts = np.zeros(shape=(62, 5), dtype=dtype)
        for idx, char in enumerate(s_text):
            i_char = ord(char)
            
            if 97 <= i_char < 123:
                i_char -= 87
            elif 65 <= i_char < 91:
                i_char -= 29
            elif 48 <= i_char < 58:
                i_char -= 48
            else:
                continue

            np_char = np_row_counts[i_char]
            np_char[0] += 1
            if idx < t_begin[1]:
                np_char[1] += 1
            if t_begin_mid[0] <= idx < t_begin_mid[1]:
                np_char[2] += 1
            if t_mid_end[0] <= idx < t_mid_end[1]:
                np_char[3] += 1
            if t_end[0] <= idx:
                np_char[4] += 1

    return np_row_counts


cpdef np.ndarray list_count_chars(list a_items, bint b_case_insensitive=True, dtype=int):
    ''' Count the instance of the numbers and letters in a list of clean, lowercase ascii strings
        Arguments:
            a_items: {list} of strs to count chars of
            b_case_insensitive: {bool} whether to count "a" and "A" as the same
            d_type: {type} the number data type to use for the count variables.
                    You can use standard int, or numpy np.uint8 or np.uint16 for lower memory usage
        Returns:
            {np.ndarray} 3d array of row index, ascii ord index, total/start/begin/begin-mid/mid-end/end counts
    '''
    cdef np.ndarray np_counts, np_row_counts, np_char
    cdef str char
    cdef int idx, i_len, i_char
    cdef (int, int) t_begin, t_begin_mid, t_mid_end, t_end
    cdef long i_row

    if b_case_insensitive:
        np_counts = np.zeros(shape=(len(a_items), 36, 5), dtype=dtype)
        
        for i_row, s_text in enumerate(a_items):
            i_len = len(s_text)
            if i_len < 100:
                t_begin, t_begin_mid, t_mid_end, t_end = a_SPLIT_POINTS[i_len]
            else:
                t_begin, t_begin_mid, t_mid_end, t_end = get_split_points(i_len)
            
            np_row_counts = np_counts[i_row]
            for idx, char in enumerate(s_text):
                i_char = ord(char)
                if 97 <= i_char < 123:
                    i_char -= 87
                elif 65 <= i_char < 91:
                    i_char -= 55
                elif 48 <= i_char < 58:
                    i_char -= 48
                else:
                    continue

                np_char = np_row_counts[i_char]
                np_char[0] += 1
                if idx < t_begin[1]:
                    np_char[1] += 1
                if t_begin_mid[0] <= idx < t_begin_mid[1]:
                    np_char[2] += 1
                if t_mid_end[0] <= idx < t_mid_end[1]:
                    np_char[3] += 1
                if t_end[0] <= idx:
                    np_char[4] += 1

    else:
        np_counts = np.zeros(shape=(len(a_items), 62, 5), dtype=dtype)
            
        for i_row, s_text in enumerate(a_items):
            i_len = len(s_text)
            if i_len < 100:
                t_begin, t_begin_mid, t_mid_end, t_end = a_SPLIT_POINTS[i_len]
            else:
                t_begin, t_begin_mid, t_mid_end, t_end = get_split_points(i_len)

            np_row_counts = np_counts[i_row]
            for idx, char in enumerate(s_text):
                i_char = ord(char)
                if 97 <= i_char < 123:
                    i_char -= 87
                elif 65 <= i_char < 91:
                    i_char -= 29
                elif 48 <= i_char < 58:
                    i_char -= 48
                else:
                    continue

                np_char = np_row_counts[i_char]
                np_char[0] += 1
                if idx < t_begin[1]:
                    np_char[1] += 1
                if t_begin_mid[0] <= idx < t_begin_mid[1]:
                    np_char[2] += 1
                if t_mid_end[0] <= idx < t_mid_end[1]:
                    np_char[3] += 1
                if t_end[0] <= idx:
                    np_char[4] += 1

    return np_counts


cpdef int count_differences(np.ndarray np_row_counts1, np.ndarray np_row_counts2):
    ''' Count differences between two sets of character counts looking only at occurences, not placement
        Arguments:
            np_row_counts1: {np.ndarray} of counts per char like (total, begin, mid1, mid2, end)
                            for each important character (number and letter) from count_chars(clean_text1)
            np_row_counts2: {np.ndarray} of counts per char like (total, begin, mid1, mid2, end)
                            for each important character (number and letter) from count_chars(clean_text2)
        Returns:
            {int} the sum of differences
    '''
    cdef np.ndarray t_char1, t_char2
    return sum([
        abs(int(t_char1[0]) - int(t_char2[0]))
        for t_char1, t_char2 in zip(np_row_counts1, np_row_counts2)])


cdef class Fuzzy:
    ''' Fuzzy matching on length, ngram jaccard, levenshtein, and phonetic '''
    cdef public float _length_weight, _edit_weight, _ngram_weight, _phonetic_weight, _partial_weight
    cdef public float _total_weights, _char_order_factor
    cdef public list tokens, tokens_bare, tokens_phon
    cdef public np.ndarray _counts
    #cdef public array.array _hashes
    cdef public np.ndarray _hashes
    cdef public dict _comparisons, _indexes
    cdef public int _range_size, _min_matches, _max_len, _max_hash
    cdef public float _fuzziness, _size_difference
    cdef public int _max_important_edits, _max_normal_edits, _max_total_edits
    cdef public bint phonetic_hash, order_sensitive
    cdef public dict _starts, _ends
    cdef public type _count_dtype
        
    def __init__(self,
        tokens,  # list of texts to compare to
        i_range_size=100,  # max difference in hash for search
        #min_matches = None,  # min integer count or float % of tokens for similar
        d_fuzziness=0.6,  # comparison % for fuzziness
        d_size_difference=0.25,  # max % difference in size to consider for fuzzy match
        i_max_important_edits=2,  # max edits in the important class
        i_max_normal_edits=4,  # max normal char edits
        i_max_total_edits=6,  # max total non-punct edits
        b_already_clean=False,  # passing in an already cleaned list of tokens
        b_phonetic_hash=True,  # hash on phonetic rather than original value
        b_order_sensitive=False,  # the order of the characters in each string matters for hashing
        d_char_order_factor=0.25,  # how sensitive the hash is to character order
        count_dtype=int,  # the datatype to store per-character counts
    ):
        self.tokens = []
        #self.tokens_clean = []
        self.tokens_bare = []
        self.tokens_phon = []
        
        self._count_dtype = count_dtype
        
        #self._counts = []
        self._counts = np.zeros(shape=(0, 36, 5), dtype=self._count_dtype)
        #self._hashes = array.array('i', [])
        self._hashes = np.zeros(shape=(0), dtype=self._count_dtype)
        self._indexes = {}
        self._comparisons = {}            
        
        self._starts = {i_len: defaultdict(list) for i_len in range(1, 4)}
        self._ends = {i_len: defaultdict(list) for i_len in range(1, 4)}

        self._range_size = int(i_range_size)
        
        self._fuzziness = d_fuzziness
        self._size_difference = d_size_difference
        self._max_important_edits = i_max_important_edits
        self._max_normal_edits = i_max_normal_edits
        self._max_total_edits = i_max_total_edits
        self.phonetic_hash = b_phonetic_hash
        self.order_sensitive = b_order_sensitive
        self._char_order_factor = d_char_order_factor
        
        self._length_weight = 0.25
        self._edit_weight = 1.25
        self._ngram_weight = 1.25
        self._phonetic_weight = 1.25
        self._partial_weight = 1.0
        
        self._total_weights = sum([
            self._length_weight,
            self._edit_weight,
            self._ngram_weight,
            self._phonetic_weight,
            self._partial_weight])
        
        self._max_len = 0
        self._max_hash = 0
        
        #print('add original tokens to internal lists')
        self.add_tokens(tokens, b_already_clean=b_already_clean, b_clear_old=True)
    
    cpdef int _hash(self, str s_value='', str s_phonetic='', str s_bare=''):
        ''' Turn value into a hash by summing the character importance '''
        cdef str s_c
        cdef int idx, i_importance
        cdef list a_importance, a_phon_import
        
        if not s_bare:
            if s_value:
                s_bare = self._bare_clean(s_value.lower())
            else:
                s_bare = s_phonetic
        
        if self.phonetic_hash:
            if not s_phonetic:
                try: s_phonetic = o_METAPHONE.phonetics(s_bare)
                except: s_phonetic = s_value
            if not s_phonetic: return 0
            a_phon_import = [e_CHAR_IMPORTANCE.get(s_c, 0) for s_c in s_phonetic]
            #if self.order_sensitive:
            #    a_phon_import = [
            #        i_importance * ((idx + 1) ** self._char_order_factor)
            #        for idx, i_importance in enumerate(reversed(a_phon_import))]
        else:
            a_importance = [e_CHAR_IMPORTANCE.get(s_c, 0) for s_c in s_bare]
            
            if s_bare[-3:] == 'ing':
                a_importance[-2] = 8
                a_importance[-1] = 8
            elif s_bare[-2:] in {"ed", "er", "es"}:
                a_importance[-1] = 7
            elif s_bare[-1:] == "s":
                a_importance[-1] = 6
            
            if self.order_sensitive:
                a_importance = [
                    i_importance * ((idx + 1) ** self._char_order_factor)
                    for idx, i_importance in enumerate(reversed(a_importance))]
        
        if self.phonetic_hash:
            #return int(sum(a_importance) * .25) + sum(a_phon_import)
            return sum(a_phon_import)
        else:
            return sum(a_importance)
    
    cpdef str _clean(self, str token):
        return clean_text(
            token,
            b_accents=True,
            b_lowercase=True,
            b_punctuation=True,
            b_trademarks=True,
            b_html_characters=True,
            b_ampersands=True,
            b_periods=True,)
    
    cpdef str _bare(self, str token):
        return remove_all_punctuation(
            token,
            b_keepspaces=False,
            b_dehyphenate_phrases=False,)
    
    cpdef str _bare_clean(self, str token):
        return remove_all_punctuation(
            clean_text(
                token,
                b_accents=True,
                b_lowercase=True,
                b_punctuation=True,
                b_trademarks=True,
                b_html_characters=True,
                b_ampersands=True,
                b_periods=True,
            ),
            b_keepspaces=False,
            b_dehyphenate_phrases=False,)

    cpdef add_tokens(self, a_tokens, bint b_clear_old=False, bint b_already_clean=False):
        ''' add new tokens to internal lists and hashes. Tokens should already be unique.
            Arguments:
                a_tokens: {list} of string tokens
                b_clear_old: {bool} clear out existing lists
                b_already_clean: {bool} the string tokens are already cleaned of punctuation/accents
        '''
        cdef int idx, i_len, i_hash
        cdef long i_offset, i_token, i_item
        cdef str s_tok, s_phon, s_bare
        cdef list a_clean, a_bare, a_phon
        cdef (int, int, int, (int, int, int, int, int)) t_char
        cdef np.ndarray np_counts, np_char
        cdef np.ndarray np_hashes
        
        if a_tokens:
            if b_clear_old:
                self.tokens = []
                #self.tokens_clean = []
                self.tokens_bare = []
                self.tokens_phon = []
                self._counts = np.zeros(shape=(0, 36, 5), dtype=self._count_dtype)
                #self._hashes = array.array('i', [])
                self._hashes = np.zeros(shape=(0), dtype=np.uint16)
                self._indexes = {}
                self._comparisons = {}
                self._starts = {i_len: defaultdict(list) for i_len in range(1, 4)}
                self._ends = {i_len: defaultdict(list) for i_len in range(1, 4)}
                i_offset = 0
                self._max_hash = 0
            else:
                i_offset = len(self.tokens)

            if b_already_clean:
                a_clean, a_bare = a_tokens, a_tokens
            else:
                #a_clean = [self._clean(s_tok) for s_tok in a_tokens]
                #a_bare = [self._bare(s_tok) if s_tok else '' for s_tok in a_clean]
                a_bare = [self._bare_clean(s_tok) if s_tok else '' for s_tok in a_tokens]
                
            a_phon = []
            for i_token, s_tok in enumerate(a_bare):
                #try: a_phon.append(o_METAPHONE.phonetics(s_tok).lower())
                try: a_phon.append(o_METAPHONE.phonetics(s_tok))
                except: a_phon.append('')
                
                for i_len in range(1, min(4, len(s_tok))):
                    self._starts[i_len][s_tok[:i_len]].append(i_token)
                    self._ends[i_len][s_tok[-i_len:]].append(i_token)
                
            if self.phonetic_hash:
                #print('counting phonetic chars')
                np_counts = list_count_chars(a_phon, dtype=self._count_dtype)
            else:
                #print('counting  chars')
                np_counts = list_count_chars(a_tokens, dtype=self._count_dtype)
            
            #a_hashes = array.array('i', [
            #    #self._hash(s_tok, s_clean, s_phon)
            #    self._hash(s_tok, s_phon, s_bare)
            #    #for s_tok, s_clean, s_phon in zip(a_tokens, a_clean, a_bare)
            #    for s_tok, s_phon, s_bare in zip(a_tokens, a_phon, a_bare)
            #])
            np_hashes = np.array([
                self._hash(s_tok, s_phon, s_bare)
                for s_tok, s_phon, s_bare in zip(a_tokens, a_phon, a_bare)
            ], dtype=np.uint16)
            
            #print('loop through counts and tokens')
            for i_token, (np_row_counts, i_hash, s_tok) in enumerate(zip(np_counts, np_hashes, a_tokens)):
                i_item = i_offset + i_token
                i_len = int(sum([np_char[0] for np_char in np_row_counts]))
                self._max_len = max(len(s_tok), self._max_len)
                for idx, np_char in enumerate(np_row_counts):
                    #print('loop through chars')
                    if np_char[0]:
                        t_char = (i_len, idx, i_hash, tuple(np_char))
                        
                        if t_char not in self._indexes:
                            self._indexes[t_char] = [i_item]
                        else:
                            self._indexes[t_char].append(i_item)
            
            
            if b_clear_old:
                self.tokens = list(a_tokens)
                #self.tokens_clean.extend(a_clean)
                self.tokens_bare = a_bare
                self.tokens_phon = a_phon
                self._counts = np_counts
                self._hashes = np_hashes
                self._max_hash = max(self._hashes)
            else:
                self.tokens.extend(list(a_tokens))
                #self.tokens_clean.extend(a_clean)
                self.tokens_bare.extend(a_bare)
                self.tokens_phon.extend(a_phon)
                self._counts = np.concatenate((self._counts, np_counts))
                self._hashes = np.concatenate((self._hashes, np_hashes))
                #array.extend(self._hashes, a_hashes)
                self._max_hash = max(self._max_hash, max(np_hashes))

        return self
    
    cpdef list _get_all_adjacent(self,
        int i_hash, np.ndarray np_row_counts, float d_size_difference=0.5,
        int i_max_hash_diff=-1, int i_min_overlap=2, float d_min_overlap=0.25
    ):
        ''' Find all the tokens that share character counts in common with search char counts
            Filter down to the specified minimum amount of overlap
            Arguments:
                np_row_counts: {np.ndarray} of important character counts
                d_size_difference: {float} max difference in text sizes to consider adjacent
                i_max_hash_diff: {int} max difference in hash values to consider adjacent
                i_min_overlap: {int} minimum number of character counts to share in common
                d_min_overlap: {float} minimum percentage of character counts to share in common
            Returns:
                {list} of token indexes, and the number of char counts that match perfectly
        '''
        cdef int idx, i_ct, i_match, i_len, i_lower_size, i_upper_size
        cdef long i_token
        cdef np.ndarray np_char
        cdef dict e_matches
        cdef int i_min_hash, i_max_hash, i_hsh
        cdef tuple t_cts
        
        e_matches = {}
        
        i_len = int(sum([np_char[0] for np_char in np_row_counts]))
        
        d_size_difference = self._size_difference if d_size_difference < 0 else d_size_difference
        if self.phonetic_hash:
            i_lower_size  = min(i_len - self._max_important_edits, int(i_len * (1 - d_size_difference)))
            i_upper_size = max(i_len + self._max_important_edits, int(i_len * (1 + d_size_difference))) + 1
        else:
            i_lower_size  = min(i_len - self._max_normal_edits, int(i_len * (1 - d_size_difference)))
            i_upper_size = max(i_len + self._max_normal_edits, int(i_len * (1 + d_size_difference))) + 1
        
        i_max_hash_diff = self._range_size if i_max_hash_diff < 0 else i_max_hash_diff
        i_min_hash = np.uint16(max(0, i_hash - i_max_hash_diff))
        i_max_hash = np.uint16(min(self._max_hash, i_hash + i_max_hash_diff))
        
        i_min_overlap = max(i_min_overlap, int(d_min_overlap * i_len))
        #print('i_min_hash', i_min_hash)
        #print('i_max_hash', i_max_hash)
        
        #print('i_lower_size', i_lower_size)
        #print('i_upper_size', i_upper_size)
        
        #print('i_min_overlap', i_min_overlap)
        
        for idx, np_char in enumerate(np_row_counts):
            t_cts = tuple(np_char)
            for i_len in range (i_lower_size, i_upper_size):
                for i_hsh in range(i_min_hash, i_max_hash + 1):
                    for i_token in self._indexes.get((i_len, idx, i_hsh, t_cts), []):
                        if i_token in e_matches:
                            e_matches[i_token] += np_char[0]
                        else:
                            e_matches[i_token] = np_char[0]
            
        #print('e_matches', len(e_matches))
        
        #if any([self.tokens[i_token] == 'ghoulish' for i_token in e_matches.keys()]):
        #    print('ghoulish in e_matches')
        
        if i_min_overlap >= 0:
            return [
                (i_token, i_match)
                for i_token, i_match in e_matches.items()
                if i_match >= i_min_overlap]
        else:
            return list(e_matches.items())
    
    cpdef list get_adjacent(self,
        str s_token, float d_size_difference=0.5,
        int i_min_overlap=2, float d_min_overlap=0.5, int i_char_diff=-1
    ):
        ''' Find all the tokens that share character counts in common with search s_token
            Filter down to the specified minimum amount of overlap
            Arguments:
                s_token: {str} text to search for
                d_size_difference: {float} max difference in text sizes to consider adjacent
                i_min_overlap: {int} minimum number of character counts to match perfectly on
                d_min_overlap: {float} minimum percentage of character counts to share in common
                i_char_diff: {int} max number of character differences
            Returns:
                {list} of token indexes, and their measured character differences
        '''
        cdef str s_bare, s_phone
        cdef list a_adjacent, a_matches
        cdef np.ndarray np_row_counts
        cdef long i_token
        
        if self.phonetic_hash:
            s_bare = self._bare_clean(s_token)
            s_phon = o_METAPHONE.phonetics(s_bare).lower()
            np_row_counts = count_chars(s_phon, dtype=self._count_dtype)
            i_hash =  self._hash(s_value=s_token, s_phonetic=s_phon, s_bare=s_bare)
        else:
            np_row_counts = count_chars(s_token, dtype=self._count_dtype)
            i_hash =  self._hash(s_value=s_token, s_phonetic="", s_bare="")
        
        a_adjacent = self._get_all_adjacent(
            i_hash, np_row_counts,
            d_size_difference=d_size_difference,
            i_min_overlap=i_min_overlap, d_min_overlap=d_min_overlap)
        
        a_matches = [
            (i_token, count_differences(np_row_counts, self._counts[i_token]))
            for i_token, i_overlap in a_adjacent]
        
        if i_char_diff >= 0:
            a_matches = [
                (i_token, i_diffs)
                for i_token, i_diffs in a_matches
                if i_diffs <= i_char_diff]
        
        #a_matches.sort(key=itemgetter(1), reverse=True)
        return a_matches
    
    cpdef tuple _get_all_partial(self, int i_hash, str s_token, str s_bare, int i_max_extra_chars=-1):
        ''' find any tokens that start or end exactly with the specified token
            Arguments:
                s_token: {str} text to search for
                i_max_extra_chars: {int} how much longer the matches can be than the search term
            Returns:
                {list} of token indexes, and their length differences to the search term
        '''
        cdef str s_token_lower, s_start, s_end
        cdef list a_starts_with, a_ends_with
        cdef int i_len, i_lower_size, i_upper_size
        
        i_len = min(3, len(s_bare))
        i_lower_size = i_len + 1
        i_upper_size = i_len + i_max_extra_chars if i_max_extra_chars > 0 else self._max_len
        #print('i_len', i_len, 'i_lower_size', i_lower_size, 'i_upper_size', i_upper_size)
        s_token_lower = s_token.lower()
        #print('s_token_lower', s_token_lower)
        s_start, s_end = s_bare[:i_len], s_bare[-i_len:]
        #print('s_start, s_end', s_start, s_end)
        
        a_starts_with, a_ends_with = [], []
        for i_token in self._starts.get(i_len, {}).get(s_start, []):
            #print('checking start', self.tokens[i_token], i_token,
            #    len(self.tokens[i_token]) <= i_upper_size,
            #    self.tokens[i_token].lower().startswith(s_token_lower))
            if len(self.tokens[i_token]) <= i_upper_size and self.tokens[i_token].lower().startswith(s_token_lower):
                #print('start matched', self.tokens[i_token], i_token)
                a_starts_with.append(i_token)
        for i_token in self._ends.get(i_len, {}).get(s_end, []):
            #print('checking end', self.tokens[i_token], i_token,
            #    len(self.tokens[i_token]) <= i_upper_size,
            #    self.tokens[i_token].lower().endswith(s_token_lower), s_token_lower, self.tokens[i_token].lower())
            if len(self.tokens[i_token]) <= i_upper_size and self.tokens[i_token].lower().endswith(s_token_lower):
                #print('end matched', self.tokens[i_token], i_token)
                a_ends_with.append(i_token)
    
        #print('a_starts_with', a_starts_with)
        #print('a_ends_with', a_ends_with)
        i_len = len(s_token)
        a_starts_with = [
            (i_token, len(self.tokens[i_token]) - i_len)
            for i_token in a_starts_with]
        a_ends_with = [
            (i_token, len(self.tokens[i_token]) - i_len)
            for i_token in a_ends_with]
        
        return a_starts_with, a_ends_with
    
    
    cpdef list get_partials(self, str s_token, int i_max_extra_chars=-1):
        ''' find any tokens that start or end exactly with the specified token
            Arguments:
                s_token: {str} text to search for
                i_max_extra_chars: {int} how much longer the matches can be than the search term
            Returns:
                {list} of token indexes, and their length differences to the search term
        '''
        cdef str s_bare, s_token_lower
        cdef list a_starts_with, a_ends_with, a_matches
        cdef int i_hash, i_len, i_lower_size, i_upper_size
        
        if self.phonetic_hash:
            s_bare = self._bare_clean(s_token)
            i_hash =  self._hash(s_value=s_token, s_bare=s_bare) #, s_phonetic=s_phon,)
        else:
            i_hash =  self._hash(s_value=s_token) #, s_phonetic="", s_bare="")
        
        a_starts_with, a_ends_with = self._get_all_partial(
            i_hash, s_token, s_bare, i_max_extra_chars=i_max_extra_chars)
        a_matches = a_starts_with
        a_matches.extend(a_ends_with)
        
        #a_matches.sort(key=lambda t_match: t_match[1])
        return a_matches
    
    cpdef list search(self,
        str s_token, int i_max_matches=-1, float d_threshold=-1,
        int i_max_total_edits=-1, float d_size_difference=0.5, float d_min_overlap=-1,
        int i_max_hash_diff=-1, bint b_override_exact=False,
        bint b_starts_with=False, bint b_ends_with=False
    ):
        ''' list closest matches that meet minimum criteria in descending order of similarity
            Arguments:
                s_token: {str} text to search for
                i_max_matches: {int} most matches to include (doesn't not improve performance)
                d_threshold: {float} minimum similarity (overrides ._fuzziness)
                i_max_total_edits: {int} max total edits between token and alternative
                d_size_difference: {float} max difference in text sizes to consider adjacent
                d_min_overlap: {float} minimum percentage of character position counts that must match exactly
                i_max_hash_diff: {int} max difference between hash values
                b_override_exact: {bool} whether to halt and return just an exact match
                b_starts_with: {bool} whether to include longer matches that start with the search term
            Returns:
                {list} of tuples of (similar_token, d_similarity)
        '''
        cdef str s_lower, s_bare, s_phon, s_tk, s_br, s_ph
        cdef list a_possible, a_adjacent, a_starts_with, a_ends_with
        cdef np.ndarray np_row_counts
        cdef int i_important, i_other, i_unimportant, i_hash, i_min_overlap, i_len
        cdef long idx
        cdef float d_similarity
        cdef (int, float) t_overlap
        
        if d_threshold == -1: d_threshold = self._fuzziness
        
        s_lower = s_token.lower()
        s_bare = self._bare_clean(s_lower)
        cdef set c_exact_matches = {s_lower, s_bare}
        
        s_phon = ''
        try: s_phon = o_METAPHONE.phonetics(s_bare).lower()
        #except: s_phon = s_clean
        except: s_phon = s_bare
        
        
        if self.phonetic_hash:
            #print('s_phon', s_phon)
            np_row_counts = count_chars(s_phon, dtype=self._count_dtype)
            i_len = len(s_phon)
        else:
            np_row_counts = count_chars(s_bare, dtype=self._count_dtype)
            i_len = len(s_bare)
        
        if i_len < len(a_MIN_OVERLAP):
            t_overlap = a_MIN_OVERLAP[i_len]
        else:
            t_overlap = a_MIN_OVERLAP[-1]
            
        if d_min_overlap < 0:
            i_min_overlap, d_min_overlap = t_overlap
        else:
            i_min_overlap = t_overlap[0]
                
        i_hash = self._hash(s_value=s_token, s_phonetic=s_phon, s_bare=s_bare)
        
        i_max_total_edits = self._max_total_edits if i_max_total_edits < 0 else i_max_total_edits
        i_max_hash_diff = self._range_size if i_max_hash_diff < 0 else i_max_hash_diff
        
        #print('get adjacent')
        #print('i_hash', i_hash, 'i_min_overlap', i_min_overlap, '\nnp_row_counts', np_row_counts)
        a_adjacent = self._get_all_adjacent(
            i_hash, np_row_counts, d_size_difference=d_size_difference, d_min_overlap=d_min_overlap,
            i_max_hash_diff=i_max_hash_diff, i_min_overlap=i_min_overlap)
        
        if b_starts_with or b_ends_with:
            a_starts_with, a_ends_with = self._get_all_partial(i_hash, s_token, s_bare)
            if b_starts_with and a_starts_with:
                a_adjacent.extend(a_starts_with)
            if b_ends_with and a_ends_with:
                a_adjacent.extend(a_ends_with)
            
        a_possible = []
        for idx, i_char_diffs in a_adjacent:
            if count_differences(np_row_counts, self._counts[idx]) > i_max_total_edits:
                #print('too many edits', count_differences(np_row_counts, self._counts[idx]))
                continue
            
            #s_tk, s_cl, s_br = self.tokens[idx], self.tokens_clean[idx], self.tokens_bare[idx]
            s_tk, s_br = self.tokens[idx], self.tokens_bare[idx]
            s_ph = self.tokens_phon[idx]
            
            #tn_edits = edits(s_clean, s_cl)
            tn_edits = edits(s_token, s_tk)
            
            i_important, i_other, i_unimportant = count_important_edits(tn_edits)
            if i_important > self._max_important_edits or i_other > self._max_normal_edits:
                #print('too many important or normal edits', count_differences(np_row_counts, self._counts[idx]))
                continue
            
            if not b_override_exact and s_token == s_tk: return [(s_tk, 1.0)]
            
            d_similarity = self._compare(s_token, s_tk, s_phon, s_ph)
            
            # TODO: check speed between these rows and the set checks below
            #if s_lower == s_tk or s_lower == s_cl or s_lower == s_br:
            #    a_possible.append((s_tk, d_similarity))
            #    
            #elif s_clean == s_tk or s_clean == s_cl or s_clean == s_br:
            #    a_possible.append((s_tk, d_similarity))
            #    
            #elif s_bare == s_tk or s_bare == s_cl or s_bare == s_br:
            #    a_possible.append((s_tk, d_similarity))
            
            #if s_tk in c_exact_matches or s_cl in c_exact_matches or s_br in c_exact_matches:
            if s_tk in c_exact_matches or s_br in c_exact_matches:
                a_possible.append((s_tk, d_similarity))
            else:
                if d_similarity >= d_threshold:
                    a_possible.append((s_tk, d_similarity))
                    
        if a_possible:
            if i_max_matches > 0:
                return sorted(a_possible, key=itemgetter(1), reverse=True)[:i_max_matches]
            else:
                return sorted(a_possible, key=itemgetter(1), reverse=True)
        else:
            return None
            
    cpdef find(
        self, str s_token, float d_threshold=-1, int i_max_total_edits=-1,
        float d_size_difference=0.5, float d_min_overlap=0.25, int i_max_hash_diff=-1,
        bint b_starts_with=False, bint b_ends_with=False,
    ):
        ''' find closest match that meets minimum criteria
            Arguments:
                s_token: {str} text to search for
                d_threshold: {float} minimum similarity (overrides ._fuzziness)
                i_max_total_edits: {int} max total edits between token and alternative
                d_size_difference: {float} max difference in text sizes to consider adjacent
                d_min_overlap: {float} minimum percentage of character position counts that must match exactly
                i_max_hash_diff: {int} max difference between hash values
                b_starts_with: {bool} whether to include longer matches that start with the search term
                b_ends_with: {bool} whether to include longer matches that end with the search term
            Returns:
                {str} of closest token to search term
        '''
        cdef list a_possible
        a_possible = self.search(
            s_token, 1, d_threshold, i_max_total_edits=i_max_total_edits,
            d_size_difference=d_size_difference, d_min_overlap=d_min_overlap, i_max_hash_diff=i_max_hash_diff,
            b_starts_with=b_starts_with, b_ends_with=b_ends_with)
        if a_possible:
            return a_possible[0][0]
            
    cpdef float compare(self, s_word1, s_word2):
        s_word1 = str(s_word1)
        s_word2 = str(s_word2)
        return self._compare(s_word1, s_word2)
    
    cdef float _compare(self, str s_word1, str s_word2, str s_phon1='', str s_phon2=''):
        cdef str s_lower1, s_lower2, s_punc1, s_punc2
        cdef int i_len1, i_len2, i_phonetic
        cdef float d_len, d_phonetic, d_jaccard, d_partial, d_similarity
        cdef list a_similarities
        cdef tuple t_sim, t_wts, t_scores
        
        if not s_word1 or not s_word2: return 0.0
        i_len1, i_len2 = len(s_word1), len(s_word2)
        d_len = min(i_len1, i_len2) / float(max(i_len1, i_len2))
        s_lower1, s_lower2 = s_word1.lower(), s_word2.lower()
        
        # levenshtein-damerau similarity
        tn_edits = edits(s_word1, s_word2, ignore_punctuation=True)
        i_edits = sum(len(x) for x in tn_edits)
        d_lev = 1 - (i_edits / max(i_len1, i_len2))
        
        # phonetic similarity
        if not s_phon1:
            try: s_phon1 = o_METAPHONE.phonetics(self._bare_clean(s_word1))
            except: s_phon1 = ''
        if not s_phon2:
            try: s_phon2 = o_METAPHONE.phonetics(self._bare_clean(s_word2))
            except: s_phon2 = ''
        
        if s_phon1 and s_phon2:
            i_phonetic = distance(s_phon1, s_phon2)
            d_phonetic = 1 - (i_phonetic / float(max(len(s_phon1), len(s_phon2))))
        else:
            d_phonetic = 0.0
        
        s_punc1 = re_PUNCT.sub("", s_lower1)
        s_punc2 = re_PUNCT.sub("", s_lower2)
        
        # ngram similarity
        d_jaccard = jaccard(
           all_ngrams_flat(list(s_punc1), i_max_size=4, i_min_size=1),
           all_ngrams_flat(list(s_punc2), i_max_size=4, i_min_size=1))
        
        # partial word similarity
        if s_word1 == s_word2[:i_len1] or s_word1 == s_word2[-i_len1:]:
            d_partial = 1.0
        elif s_word2 == s_word1[:i_len2] or s_word2 == s_word1[-i_len2:]:
            d_partial = 1.0
        elif s_lower1 == s_lower2[:i_len1] or s_lower1 == s_lower2[-i_len1:]:
            d_partial = 1.0
        elif s_lower2 == s_lower1[:i_len2] or s_lower2 == s_lower1[-i_len2:]:
            d_partial = 1.0
        else:
            d_partial = 0.0

        a_similarities = [
            (d_len, self._length_weight, d_len * self._length_weight),
            (d_lev, self._edit_weight, d_lev * self._edit_weight),
            (d_jaccard, self._ngram_weight, d_jaccard * self._ngram_weight),
            (d_phonetic, self._phonetic_weight, d_phonetic * self._phonetic_weight),
            (d_partial, self._partial_weight, d_partial * self._partial_weight),
        ]
        
        a_similarities.sort(reverse=True, key=itemgetter(0))
        t_sim, t_wts, t_scores = zip(*a_similarities)
        d_similarity = sum(t_scores[:-1]) / sum(t_wts[:-1])

        return d_similarity
    
    cpdef float _inconsistent_expression(self, long idx1, long idx2):
        ''' Test to see if token1 and token2 are variants of each other'''
        cdef int i_phon_dist, i_important, i_other, i_unimportant
        
        i_phon_dist = distance(self.tokens_phon[idx1], self.tokens_phon[idx2])
        if i_phon_dist > self._max_total_edits: return 0.0

        #tn_edits = edits(self.tokens_clean[idx1], self.tokens_clean[idx2])
        tn_edits = edits(self.tokens[idx1], self.tokens[idx2])
        i_important, i_other, i_unimportant = count_important_edits(tn_edits)
        if i_important > self._max_important_edits or i_other > self._max_normal_edits: return 0.0

        return self._compare(self.tokens[idx1], self.tokens[idx2], self.tokens_phon[idx1], self.tokens_phon[idx2])
    
    cpdef list _analyze_hash_groups(
        self, float d_threshold=-1,
        float d_size_difference=0.75, int i_min_overlap=2, float d_min_overlap=0.75,
        int i_max_hash_diff=75
    ):
        ''' find all the alternates in the tokens as indexes '''
        cdef dict e_similarities, e_similars
        cdef list a_equivalents, a_adjacent, a_alts
        cdef long i_range
        cdef int idx, sim_idx, i_hash, i_overlap
        cdef float d_similarity
        cdef np.ndarray np_row_counts
        
        if d_threshold == -1: d_threshold = self._fuzziness
        
        e_similarities = {}
        a_equivalents = []
        for idx, (i_hash, np_row_counts) in enumerate(zip(self._hashes, self._counts)):
            a_adjacent = self._get_all_adjacent(
                i_hash, np_row_counts, d_size_difference=d_size_difference,
                i_max_hash_diff=i_max_hash_diff,
                d_min_overlap=d_min_overlap, i_min_overlap=i_min_overlap)
            
            e_similars = {}
            for sim_idx, i_overlap in a_adjacent:
                if sim_idx == idx: continue
                d_similarity = e_similarities.get((idx, sim_idx), 0)
                if not d_similarity:
                    d_similarity = self._inconsistent_expression(idx, sim_idx)
                    
                    e_similarities[(idx, sim_idx)] = d_similarity
                    e_similarities[(sim_idx, idx)] = d_similarity
                
                if d_similarity >= d_threshold:
                    e_similars[sim_idx] = d_similarity
            
            a_adjacent = sorted(e_similars.items(), key=itemgetter(1), reverse=True)
            a_equivalents.append(a_adjacent)

        return a_equivalents
    
    cpdef dict alternates(
        self, float d_threshold=0.75, float d_size_difference=0.5,
        float d_min_overlap=0.25, i_min_overlap=1, int i_max_hash_diff=50
    ):
        ''' find all the alternates in the tokens '''
        cdef list a_alternates, a_alts
        cdef dict e_expressions
        cdef int idx, sim_idx
        cdef float d_sim
        
        a_alternates = self._analyze_hash_groups(
            d_threshold=d_threshold,
            d_size_difference=d_size_difference,
            d_min_overlap=d_min_overlap,
            i_min_overlap=i_min_overlap,
            i_max_hash_diff=i_max_hash_diff)
        
        e_expressions = {}
        for idx, a_alts in enumerate(a_alternates):
            e_expressions[self.tokens[idx]] = {
                self.tokens[sim_idx]: d_sim
                for sim_idx, d_sim in a_alts
            }
        return e_expressions
    
    def save_dict_file(self,
        dictionary_npz_file,
        b_save_bare=False,
        b_save_phonetic=True,
        b_save_counts=True,
        b_save_hashes=True,
        b_save_indexes=True,
        b_save_starts_ends=True,
        _count_dtype=np.uint8,
        _hash_dtype=np.uint16,
    ):
        ''' save dictionary variables to a file
            Arguments:
                dictionary_npz_file: {str} or file object to write numpy.arrays
                    They will be saved using savez_compressed as named kwargs of
                    "counts", "tokens", "bare", "phone", "hashes"
                b_save_bare: {bool} save bare version of tokens
                b_save_phonetic: {bool} save phonetic version of tokens
                b_save_counts: {bool} save char count array
                b_save_hashes: {bool} save hash array
                b_save_indexes: {bool} save dict with indexes
                b_save_starts_ends: {bool} save starts/ends dictionaries
                _count_dtype: {type} to save the number counts
        '''
        #n_start = time()
        
        e_to_save = {
            "tokens": np.array(list(self.tokens), dtype=str),
            "phonetic_hash": self.phonetic_hash,
            "order_sensitive": self.order_sensitive,
            "max_len": self._max_len,
            "max_hash": self._max_hash,
            "fuzziness": self._fuzziness,
            "size_difference": self._size_difference,
            "max_important_edits": self._max_important_edits,
            "max_normal_edits": self._max_normal_edits,
            "max_total_edits": self._max_total_edits,
            "char_order_factor": self._char_order_factor,
        }
        
        if b_save_bare:
            e_to_save['bare'] = np.array(list(self.tokens_bare), dtype=str)
        if b_save_phonetic:
            e_to_save['phonetic'] = np.array(list(self.tokens_phon), dtype=str)
        if b_save_counts:
            e_to_save['counts'] = self._counts.astype(_count_dtype)
        if b_save_hashes:
            e_to_save['hashes'] = np.array(list(self._hashes), dtype=_hash_dtype)
        if b_save_indexes:
            e_to_save['indexes'] = self._indexes
        if b_save_starts_ends:
            e_to_save['starts'] = self._starts
            e_to_save['ends'] = self._ends
        
        np.savez_compressed(dictionary_npz_file, **e_to_save)
        
        #print("Saving tokens and metadata to file took:", FwUtility.secondsToStr(time() - n_start))
        return self
    
    cpdef load_dict_file(self, dictionary_npz_file, b_already_clean=False, _count_dtype=int,):
        ''' Load tokens, and metadata from a saved np file. This will overwrite the existing tokens
            Arguments:
                dictionary_npz_file: {str} or file object of saved numpy.arrays
                    must at least contain tokens, but should also contain bare and phonetic versions,
                    character counts, hashes.
                    They should be saved using savez_compressed as named kwargs of
                    "counts", "tokens", "bare", "phonetic", "hashes", etc.
        '''
        cdef int idx, i_len, i_hash
        cdef long i_token
        cdef str s_tok, s_phon, s_bare
        cdef np.ndarray np_char
        cdef (int, int, int, (int, int, int, int, int)) t_char
        
        if dictionary_npz_file is None:
            raise Exception("No file passed in")
    
        o_file = np.load(dictionary_npz_file, allow_pickle=True)
    
        assert 'tokens' in o_file
        #self.tokens = [str(x) for x in o_file['tokens']]
        self.tokens = list(map(str, o_file['tokens']))
        
        if 'phonetic_hash' in o_file:
            self.phonetic_hash = o_file['phonetic_hash'].item(0)
        if 'order_sensitive' in o_file:
            self.order_sensitive = o_file['order_sensitive'].item(0)
        
        if 'fuzziness' in o_file:
            self._fuzziness = o_file['fuzziness'].item(0)
        if 'size_difference' in o_file:
            self._size_difference = o_file['size_difference'].item(0)
        if 'max_important_edits' in o_file:
            self._max_important_edits = o_file['max_important_edits'].item(0)
        if 'max_normal_edits' in o_file:
            self._max_normal_edits = o_file['max_normal_edits'].item(0)
        if 'max_total_edits' in o_file:
            self._max_total_edits = o_file['max_total_edits'].item(0)
        if 'char_order_factor' in o_file:
            self._char_order_factor = o_file['char_order_factor'].item(0)
        
        
        if 'bare' in o_file:
            #self.tokens_bare = [str(x) for x in o_file['bare']]
            self.tokens_bare = list(map(str, o_file['bare']))
        else:
            if b_already_clean:
                self.tokens_bare = self.tokens
            else:
                print('cleaning tokens')
                self.tokens_bare = [self._bare_clean(s_tok) if s_tok else '' for s_tok in self.tokens]
        
        if 'phonetic' in o_file:
            #self.tokens_phon = [str(x) for x in o_file['phonetic']]
            #self.tokens_phon = list(o_file['phonetic'])
            self.tokens_phon = list(map(str, o_file['phonetic']))
        else:
            print('making phonetics')
            self.tokens_phon = []
            for i_token, s_tok in enumerate(self.tokens_bare):
                try: self.tokens_phon.append(o_METAPHONE.phonetics(s_tok))
                except: self.tokens_phon.append('')
    
        if 'starts' in o_file and 'ends' in o_file:
            self._starts = o_file['starts'].item(0)
            self._ends = o_file['ends'].item(0)
        else:
            self._starts = {i_len: defaultdict(list) for i_len in range(1, 4)}
            self._ends = {i_len: defaultdict(list) for i_len in range(1, 4)}
            for i_token, s_tok in enumerate(self.tokens_bare):
                for i_len in range(1, min(4, len(s_tok))):
                    self._starts[i_len][s_tok[:i_len]].append(i_token)
                    self._ends[i_len][s_tok[-i_len:]].append(i_token)
            
        if 'counts' in o_file:
            #self._counts = o_file['counts'].astype(_count_dtype)
            self._counts = np.array(o_file['counts'], dtype=_count_dtype)
        else:
            print('counting characters')
            if self.phonetic_hash:
                self._counts = list_count_chars(self.tokens_phon, dtype=_count_dtype)
            else:
                self._counts = list_count_chars(self.tokens, dtype=_count_dtype)
        
        if 'hashes' in o_file:
            self._hashes = o_file['hashes']
        else:
            print('calculating hashes')
            #self._hashes = array.array('i', [
            #    self._hash(s_tok, s_phon, s_bare)
            #    for s_tok, s_phon, s_bare in zip(self.tokens, self.tokens_phon, self.tokens_bare)
            #])
            self._hashes = np.array([
                self._hash(s_tok, s_phon, s_bare)
                for s_tok, s_phon, s_bare in zip(self.tokens, self.tokens_phon, self.tokens_bare)
            ], dtype=np.uint16)
        
        if 'max_len' in o_file:
            self._max_len = o_file['max_len'].item(0)
        else:
            self._max_len = max([len(s_tok) for s_tok in self.tokens])
        
        if 'max_hash' in o_file:
            self._max_hash = o_file['max_hash'].item(0)
        else:
            self._max_hash = max([i_hash for i_hash in self._hashes])
        
        if 'indexes' in o_file:
            self._indexes = o_file['indexes'].item(0)
        else:
            print('making indexes')
            self._indexes = {}
            i_offset = 0
            for i_token, (np_row_counts, i_hash) in enumerate(zip(self._counts, self._hashes)):
                i_item = i_offset + i_token
                i_len = int(sum([np_char[0] for np_char in np_row_counts]))
                for idx, np_char in enumerate(np_row_counts):
                    if np_char[0]:
                        t_char = (i_len, idx, i_hash, tuple(np_char))
                        if t_char not in self._indexes:
                            self._indexes[t_char] = [i_item]
                        else:
                            self._indexes[t_char].append(i_item)
        
        return self
