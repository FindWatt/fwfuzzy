import numpy as np
import pyximport; pyximport.install(
    setup_args={'include_dirs': np.get_include()})

try:
    from fuzzy_match import *
except ImportError:
    from .fuzzy_match import *


__version__ = "1.1.1"

