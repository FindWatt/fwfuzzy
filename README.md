# ### Fuzzy Matching of words ### #

For finding the closest match for a single word from a list of words such as a tokenized sentence or title. This uses a combination of phonetic, levenshtein-damerau, and character ngram-jaccard, plus starts/ends with.

Example usage:

```
#!python

from FwFuzzy import Fuzzy

a_tokens = ["one", "two", "three", "four", "five"]

o_fuzzy = Fuzzy(a_tokens)

a_best_guesses = o_fuzzy.search('T.W.O.', d_minimum=0.25)

s_closest_match = o_fuzzy.find('threw', d_minimum=0.25)

```
