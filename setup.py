import os, sys

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwFuzzy/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = version_line.split('__version__ = ')[-1][1:][:-2]
            
class custom_install(install):
    def run(self):
        self.install_fw_dependencies()
        print("This is a custom installation")
        install.run(self)
        
    def install_fw_dependencies(self):
        if sys.version_info.major == 2:
            os.system("pip install git+https://bitbucket.org/FindWatt/fwtext")
            os.system("pip install git+https://bitbucket.org/FindWatt/fwfreq")
            os.system("pip install git+https://bitbucket.org/FindWatt/fweditclassifier")
        elif sys.version_info.major == 3:
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwtext")
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwfreq")
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fweditclassifier")

setuptools.setup(
    name="FwFuzzy",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwfuzzy",

    author="FindWatt",

    description="Fuzzy matching class",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx"]},
    py_modules=['FwFuzzy'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "pyphonetics",
    ],
    cmdclass={'install': custom_install}
)


